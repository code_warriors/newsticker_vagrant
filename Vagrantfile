# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/focal64"
  config.vm.box_version = "20210119.1.0"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.
  config.vm.provider "virtualbox" do |vb|
    # https://bugs.launchpad.net/cloud-images/+bug/1829625
    vb.customize ["modifyvm", :id, "--uart1", "0x3F8", "4"]
    vb.customize ["modifyvm", :id, "--uartmode1", "file", File::NULL]
    vb.customize ["modifyvm", :id, "--name", "ubuntu_newsticker_webapp"]
    vb.customize ["modifyvm", :id, "--memory", 4096]
    vb.customize ["modifyvm", :id, "--cpus", 4]
  end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update;
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC/Y6JwKIYLqPIUB7J9pkW0yaoWRwigMBn0wV7O6xV6D2qMwpyoGkdNbpZaLrVZCVQFxgpKfHtXYP2EnEy0Yhbvf1pWeHlb/hQX7gbprVR5xfcRlxe06tq6Q36qjMJygr5mSeGqKRxDh9xTYbISLDWzLrCJ0XHCGx/eYZa1LSwPDCe/exkCGAPU39NAdGwtYJnOeQW/fvQPq/ai1xj+FEiC16361XzhhzTInmhvctP9K0cUT8CsdWP624D70YBCiIdVuxwwGu0M/mETPsdKVrtcu9cnK21EPJIVxjDzL+cv287Ebnl1pXc3HfYQvdCgtriA6c90ACAPpuxnNQBZSmz3pe9CCBPd3tAoV27Zo9MwHaEqpbGRkGo2Z0fworI/8uj22AcCtojaRczzBZhNflDi1gMPWFDrb9s7rP9ZeFQ7+VoO/xnqkJo0kt4akPlB6TAahhjC+AYLfy3L0uwl9aVmyblRGSy6HWMj/jBjmIgaB4SSf26/6YuP5CBLJFluJpt+CQnoESAGvi+1wI6AKnc58yCgVtrHBObly9stW3P/nZGmLiehTAXS4IsyjPI3U8H5ZIijV5sZVF6oeJkdo7VtTH2EWYxH/BG7Tj7HoqdenR6+mV1i4ZuJUVQWzmbaHdYvoNRpjFI5X4OaocdrX15oydiAQecHsAVlP3/ZHFUEiQ== c_hueser@web.de" >> /home/vagrant/.ssh/authorized_keys;
  SHELL

end
